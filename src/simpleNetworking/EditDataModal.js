import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
  Modal,
  Pressable,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from './url';

export default function EditDataModal({
  navigation,
  route,
  show,
  onClose,
  onClick,
  onAddOrUpdate,
}) {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  var dataMobil = route?.params;

  const editData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      alert('Data Mobil berhasil dirubah');
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    if (dataMobil) {
      setNamaMobil(dataMobil.title || ''); // Set default values or handle missing properties
      setTotalKM(dataMobil.totalKM || '');
      setHargaMobil(dataMobil.harga || '');
    }
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={styles.centeredView}>
        <Modal animationType="slide" transparent={true} visible={show}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={onClose}
                  style={{
                    width: '10%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingBottom: 10,
                    paddingTop: 20,
                  }}>
                  <Icon name="arrowleft" size={20} color="#000" />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: '#000',
                    paddingTop: 10,
                  }}>
                  Edit Data
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  padding: 15,
                }}>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Nama Mobil
                  </Text>
                  <TextInput
                    placeholder="Masukkan Nama Mobil"
                    style={styles.txtInput}
                    value={namaMobil}
                    onChangeText={text => setNamaMobil(text)}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Total Kilometer
                  </Text>
                  <TextInput
                    placeholder="contoh: 100 KM"
                    style={styles.txtInput}
                    value={totalKM}
                    onChangeText={text => setTotalKM(text)}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Harga Mobil
                  </Text>
                  <TextInput
                    placeholder="Masukkan Harga Mobil"
                    style={styles.txtInput}
                    keyboardType="number-pad"
                    value={hargaMobil}
                    onChangeText={text => setHargaMobil(text)}
                  />
                </View>
                <TouchableOpacity
                  onPress={() => editData()}
                  style={styles.btnAdd}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>
                    Edit Data
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
});
