import React, {Node} from 'react';
import { StatusBar, SafeAreaView } from 'react-native';
import Routing from './src/simpleNetworking/Routing';

const App = () => {
  return(
    <SafeAreaView style={{flex:1}}>
      <StatusBar barStyle={'dark-content'}/>
      <Routing/>
    </SafeAreaView>
  );
};

export default App;
